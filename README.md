Future Projects
===============

This project is a placeholder for future projects.

The ideas are registered as [issues](https://gitlab.com/buildfunthings/future-projects/issues).

If you want to submit an idea, all you need to do is raise an issue.